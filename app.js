var express = require("express");
var app = express();
var bodyparser = require("body-parser");
var mongoose = require("mongoose");
var Campground = require("./models/campground");
var seedDB = require("./seeds");
var Comment = require("./models/comment");
var knock = require("knock-knock-jokes");

mongoose.connect("mongodb://localhost:27017/yelp_camp");

seedDB();

app.use(express.static(__dirname+"/public"));
// Campground.create(
//         {name:"Granite Hill",
//          image:"https://source.unsplash.com/pSaEMIiUO84",
//          description:"This is a giant Granite hill.No water.Just lots and lots of granite!"},
//          function(err, campground){
//              if(err){
//                  console.log(err);
//              }else{
//                  console.log("New campground has been added:");
//                  console.log(campground);
//              }
//          });

app.use(bodyparser.urlencoded({extended:true}));

app.set("view engine","ejs");

app.get("/",function(req,res){
	// var knockKnock = knock();
	res.render("landing",{knockKnock:knock()});
});

app.get("/campgrounds",function(req,res){
	Campground.find({},function(err,allCampgrounds){
		if(err){
			console.log(err);
		}else{
			res.render("campgrounds/index",{campgrounds:allCampgrounds});
		}
	})
	// res.render("campgrounds",{campgrounds:campgrounds});
});

app.post("/campgrounds",function(req,res){
   var name = req.body.name;
   var image = req.body.image;
   var desc = req.body.description;
   var newObject = {name:name,image:image,description:desc};
   
   Campground.create(newObject,function(err,newlyCreated){
	   if(err){
		   console.log(err);
	   }else{
		   res.redirect("/campgrounds");
	   }
   })

});

app.get("/campgrounds/new",function(req, res) {
   res.render("campgrounds/new");
});

app.get("/campgrounds/:id",function(req,res){
   Campground.findById(req.params.id).populate("comments").exec(function(err,foundCampground){
	  if(err){
		  console.log(err);
	  }else{
			 res.render("campgrounds/show",{foundCampground:foundCampground}); 
	  } 
   });
});

//=========COMMENT ROUTES============
app.get("/campgrounds/:id/comments/new",function(req, res) {
	Campground.findById(req.params.id,function(err,campground){
	   if(err){
		   console.log(err);
	   } else{
		   res.render("comments/new",{campground:campground});
	   }
	});
});

app.post("/campgrounds/:id/comments",function(req,res){
   Campground.findById(req.params.id,function(err, campground) {
	   if(err){
		   console.log(err);
		   res.redirect("/campgrounds");
	   }else{
		   Comment.create(req.body.comments,function(err, comment){
			  if(err){
				  console.log(err);
			  } else{
				  campground.comments.push(comment);
				  campground.save();
				  res.redirect("/campgrounds/"+campground._id);
			  }
		   });
	   }
   }) ;
});

app.listen(process.env.PORT,process.env.IP,function(){
	console.log("Server started!");
});